package org.example;

public class Main {
    public static void main(String[] args) {
        //String literal
        String stringA = "String Literal";
        String stringB = "String Literal";
        System.out.println(stringB == stringA); // true
        // Bởi vì A và B đều có giá trị là 1 địa chỉ dẫn đến đối tượng có giá trị "String Literal"
        // trong String Pool của Heap

        //String object
        String stringY = new String("String Object");
        String stringZ = new String("String Object");
        System.out.println(stringY == stringZ); // false
        System.out.println(stringY.equals(stringZ)); // true
        // Bởi vì khi sử dụng thuộc tính new, đơn gian là ta đã tạo 2 đối tượng khác nhau
        // và giá trị của chúng là địa chỉ tham chiếu đến 2 đối tượng khác nhau ở trong heap
        // và không nằm trong string pool

        System.out.println(stringA == stringZ); // false
    }
}